package indent.gts.indentifcation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import indent.gts.indentifcation.beans.Moto;
public interface MotoRepository extends JpaRepository<Moto, Integer> , JpaSpecificationExecutor<Moto>{
	@Query("SELECT u FROM Moto u WHERE u.status = :status")
	List<Moto> getAllMotos(@Param("status") short status);
}
