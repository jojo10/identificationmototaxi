package indent.gts.indentifcation.repository;

import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import indent.gts.indentifcation.beans.Users;


public interface UsersRepository extends JpaRepository<Users, Integer> , JpaSpecificationExecutor<Users> {
	
	@Query("SELECT u FROM Users u WHERE u.status = :status")
	List<Users> getAllUsers(@Param("status") short status);
	
	@Query("select u from Users u where u.login = :login and u.motPass = :motPass and u.status = :status")
	Users connectUser(@Param("login") String login, @Param("motPass") String motPass, @Param("status") short status);
	/*
	@Query("SELECT u FROM Users u WHERE u.login= :login AND u.motPass= :motPass AND u.status= :status")
	Users connectUsersWithLoginAndPass(@Param("Login") String login,@Param("motPass") String motPass,@Param("status") short status );
*/
	Users findByLogin(String login);
	Users findByMatricule(String matricule);
	Users findByNumeroCni (String numeroCni);
	Users findByTel(String tel);
}
