package indent.gts.indentifcation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import indent.gts.indentifcation.beans.Arrondissement;
public interface ArrondissementRepository extends JpaRepository<Arrondissement, Integer> , JpaSpecificationExecutor<Arrondissement> {
	
	@Query("SELECT u FROM Arrondissement u WHERE u.status = :status")
	List<Arrondissement> getAllArrondissements(@Param("status") short status);
	/*
	@Query("SELECT u FROM Arrondissement u WHERE u.departementId.departementId = :departementId AND p.status= :status")
	List<Arrondissement> getAllArrondissementByDepartements(@Param("departementId") int departementId, @Param("status") short status);
	*/
	Arrondissement findByArrondissementName(String arrondissementName);
	
}
