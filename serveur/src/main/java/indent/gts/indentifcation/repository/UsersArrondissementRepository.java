package indent.gts.indentifcation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import indent.gts.indentifcation.beans.Arrondissement;
import indent.gts.indentifcation.beans.UsersArrondissement;

public interface UsersArrondissementRepository extends JpaRepository<UsersArrondissement, Integer> , JpaSpecificationExecutor<UsersArrondissement>  {
	
	@Query("SELECT u FROM UsersArrondissement u")
	List<Arrondissement> getAllUsersArrondissement();
}
