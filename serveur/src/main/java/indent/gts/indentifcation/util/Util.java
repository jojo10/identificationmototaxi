package indent.gts.indentifcation.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Util {
	
	/**
	 * Fonction permettant de retrouver le format Json d'un objet
	 * @param obj
	 * @param ojectClass
	 * @return
	 */
	public static String getJsonFromObject (Object obj, Class ojectClass) {
		try {
			String json = null;
	    	final GsonBuilder builder = new GsonBuilder();
	        final Gson gson = builder.create();
	        json = gson.toJson(obj, ojectClass);
	    	return json;
		}
		catch (Exception e) {
			String error = "Errors while converting";
			String json = null;
	    	final GsonBuilder builder = new GsonBuilder();
	        final Gson gson = builder.create();
	        json = gson.toJson(error, String.class);
	    	return json;
		}
	}
	
	/**
	 * Cette fonction permet de retrouver la forme objet d'une chaîne Json
	 * @param jsonObject
	 * @param objectClass
	 * @return
	 */
	public static Object getObjectFromJson (String jsonObject, Class objectClass) {
		try {
			Object obj;
	    	final GsonBuilder builder = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	        final Gson gson = builder.create();
	        obj = gson.fromJson(jsonObject, objectClass);
	    	return obj;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Cette fonction envoie des données simples de type String à un serveur distant
	 * @param request
	 * @return
	 */
	public static String sendSimpleDataToRemoteServer (String request) {
		try{
			URL url = new URL(request);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			DataOutputStream paramsWriter = new DataOutputStream(conn.getOutputStream());
			paramsWriter.flush();
			paramsWriter.close();
			InputStream remoteResponse = conn.getInputStream();
			OutputStream localResponder = new OutputStream() {
				private StringBuilder string = new StringBuilder();
				
				@Override
				public void write(int x) throws IOException {
					this.string.append((char) x );
				}

				public String toString(){
					return this.string.toString();
				}
			};//.getOutputStream.getOutputStream();
			int c;
			while((c = remoteResponse.read()) != -1){
				localResponder.write(c);
			}	
			//			localResponder.close();
			//			remoteResponse.close();
			conn.disconnect();
			String answerJson = localResponder.toString();
			return answerJson;
		}catch(Exception e){
			return null;
		}
	}
	
	/**
	 * Cette fonction permet d'envoyer un objet au format Json à un server distant
	 * @param request
	 * @param jsonObject
	 * @return
	 */
	public static String sendObjectToRemoteServer (String request, String jsonObject) {
		try {
			URL url = new URL(request);
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(jsonObject);
			out.close();

			InputStream remoteResponse = connection.getInputStream();
			OutputStream localResponder = new OutputStream() {
				private StringBuilder string = new StringBuilder();
				
				@Override
				public void write(int x) throws IOException {
					this.string.append((char) x );
				}

				public String toString(){
					return this.string.toString();
				}
			};
			int c;
			while((c = remoteResponse.read()) != -1){
				localResponder.write(c);
			}
			String answerJson = localResponder.toString();
			return answerJson;
		} catch (Exception e) {
			return null;
		}
	}

	/**
     * Cette fonction permet de générer le mot de passe par défaut d'un employé.
     * @return
     */
    public static String generateEmployeePassword() {
    	Random rand = new Random();
        String password = "";
        for (int i = 0; i < 15; i++) {
            char c = (char) (rand.nextInt(60) + 69);
            password += c;
        }
        return Base64.getEncoder().withoutPadding().encodeToString(password.getBytes());
    }

}
