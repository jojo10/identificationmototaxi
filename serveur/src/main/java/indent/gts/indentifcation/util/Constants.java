package indent.gts.indentifcation.util;

public class Constants {
	
	public static final short
	STATE_ACTIVATED=0,
	STATE_DELETED=2,
	STATE_ARCHIVE=1,
	STATE_USER_PACK_CLOSED=3,
	STATE_INTERRUPT_WORK=4,
	STATE_FINISH_WORK=5;

//Durée maximale d'inactivité (30 min)
public static final int
	TIMEOUT_VALUE=30;
	
//Codes d'erreurs
public static final int
	USER_NON_AUTHENTICATED = 401,
	CONNECTION_TIMEOUT = 403,
	ERROR_PAGE_NOT_FOUND = 404,
	SERVER_ERROR = 500,
	SERVER_DENY_RESPONSE = 504;
	
//AccessRights management
public static final String
	MEMBER_ACCESS = "public",
	ADMIN_ACCESS = "administrator",
	
	MEMBER_ACCESS_RIGHT = "member_right",
	ADMIN_ACCESS_RIGHT = "administrator_right",
	ROOT_RIGHT = "All";

}
