package indent.gts.indentifcation.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import indent.gts.indentifcation.beans.Users;
import indent.gts.indentifcation.services.UsersService;
import indent.gts.indentifcation.exceptions.InvalidInputException;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/users")
public class UsersController {
	@Autowired
	private UsersService usersService;
	
	@RequestMapping(method=RequestMethod.GET, value="/getAllUsers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllUsers() {
		return new ResponseEntity<>(usersService.getAllUsers(), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/createUsers", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createUser(@RequestBody Users user) throws InvalidInputException {
		if (usersService.findByLogin(user.getLogin()) !=null ) {
			throw new InvalidInputException("Login already used");
		}
		if (usersService.findByMatricule(user.getMatricule())!=null) {
			throw new InvalidInputException("Matricule already used");
		}
		if (usersService.findByNumeroCni(user.getNumeroCni())!=null) {
			throw new InvalidInputException("ID card number already used");
		}
		if (usersService.findByTel(user.getTel())!=null) {
			throw new InvalidInputException("Phone number already used");
		}
		Users usersCreated= usersService.createUsers(user);
		return new ResponseEntity<>(usersCreated, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/updateUser", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateUser(@RequestBody Users user) throws InvalidInputException {
		Users usersByLogin= usersService.findByLogin(user.getLogin());
		Users usersByMatricule= usersService.findByMatricule(user.getMatricule());
		Users usersByTel =usersService.findByTel(user.getTel());
		Users usersByNumerCni=usersService.findByNumeroCni(user.getNumeroCni());
		
		if(usersByLogin!=null) {
			if(usersByLogin.getUsersId()!=user.getUsersId()) {
				throw new InvalidInputException("The new user login you have choosen already used");
			}
		}
		if(usersByMatricule!=null) {
			if(usersByMatricule.getUsersId()!=user.getUsersId()) {
				throw new InvalidInputException("The new user matricule you have choosen already used");
			}
		}
		if(usersByNumerCni!=null) {
			if(usersByNumerCni.getUsersId()!=user.getUsersId()) {
				throw new InvalidInputException("The new user ID card number you have choosen already used");
			}
		}
		if(usersByTel!=null) {
			if(usersByTel.getUsersId()!=user.getUsersId()) {
				throw new InvalidInputException("The new user phone number you have choosen already used");
			}
		}
		
		Users userUpdated= usersService.updateUsers(user);
		return new ResponseEntity<>(userUpdated, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/findUserById/{usersId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findUserById(@PathVariable("usersId") int usersId) {
		Users userFound= usersService.findUsersById(usersId);
		return new ResponseEntity<>(userFound, HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.GET, value="/findUserByMatricule/{matricule}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findUserByMatricule(@PathVariable("matricule") String matricule) {
		Users userFound= usersService.findByMatricule(matricule);
		return new ResponseEntity<>(userFound, HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.GET, value="/findUserByNumeroCni/{numeroCni}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findUserByNumeroCni(@PathVariable("numeroCni") String numeroCni) {
		Users userFound= usersService.findByNumeroCni(numeroCni);
		return new ResponseEntity<>(userFound, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/connectUsersWithLoginAndPass", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> connectUsersWithLoginAndPass(@RequestBody Users user) throws InvalidInputException{
		Users userToConnect= usersService.connectUser(user);
		if(userToConnect!=null) {
			return new ResponseEntity<>(userToConnect, HttpStatus.OK);
		}else {
			throw new InvalidInputException("Errors occured while connecting user");
		}
		
	}
}
