package indent.gts.indentifcation.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import indent.gts.indentifcation.beans.Arrondissement;
import indent.gts.indentifcation.beans.Departement;
import indent.gts.indentifcation.beans.Users;
import indent.gts.indentifcation.services.ArrondissementService;
import indent.gts.indentifcation.services.UsersService;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/arrondissements")
public class ArrondissementController {

	
	@Autowired
	private ArrondissementService arrondisService;
	
	@RequestMapping(method=RequestMethod.GET, value="/getAllArrondissements", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllArrondissements() {
		return new ResponseEntity<>(arrondisService.getAllArrondissements(), HttpStatus.OK);
	}
	/*
	@RequestMapping(method=RequestMethod.POST, value="/getAllArrondissementByDepartements", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllArrondissementByDepartements(@RequestBody Departement depart) {
		return new ResponseEntity<>(arrondisService.getAllArrondissementByDepartements(depart.getDepartementId()), HttpStatus.OK);
	}*/
}
