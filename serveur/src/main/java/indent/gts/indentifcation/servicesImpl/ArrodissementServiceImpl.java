package indent.gts.indentifcation.servicesImpl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import indent.gts.indentifcation.beans.Arrondissement;
import indent.gts.indentifcation.repository.ArrondissementRepository;
import indent.gts.indentifcation.services.ArrondissementService;
import indent.gts.indentifcation.util.Constants;

@Service
@Transactional
public class ArrodissementServiceImpl implements ArrondissementService{

	@Autowired
	ArrondissementRepository arrondissementRepo;
	
	@Override
	public List<Arrondissement> getAllArrondissements() {
		List<Arrondissement> arrondis = new ArrayList<Arrondissement>();
		arrondissementRepo.getAllArrondissements(Constants.STATE_ACTIVATED)                 
		.forEach(arrondis::add);
		return arrondis;
	}
	/*
	@Override
	public List<Arrondissement> getAllArrondissementByDepartements(int departementId) {
		List<Arrondissement> arrondisDepart = new ArrayList<Arrondissement>();
		arrondissementRepo.getAllArrondissementByDepartements(departementId, Constants.STATE_ACTIVATED).forEach(arrondisDepart::add);;
		return arrondisDepart;
	}*/

	

}
