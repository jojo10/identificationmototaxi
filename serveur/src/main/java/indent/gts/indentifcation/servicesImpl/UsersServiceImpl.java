package indent.gts.indentifcation.servicesImpl;

import java.util.ArrayList;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import indent.gts.indentifcation.beans.Users;
import indent.gts.indentifcation.repository.UsersRepository;
import indent.gts.indentifcation.services.UsersService;
import indent.gts.indentifcation.util.Constants;

@Service
@Transactional
public class UsersServiceImpl implements UsersService {

	@Autowired
	UsersRepository userRepo;
	
	@Override
	public Users createUsers(Users user) {
		return userRepo.save(user);
	}

	@Override
	public Users updateUsers(Users user) {
		return userRepo.save(user);
	}

	@Override
	public List<Users> getAllUsers() {
		List<Users> Users = new ArrayList<Users>();
		userRepo.getAllUsers(Constants.STATE_ACTIVATED)                 
		.forEach(Users::add);
		return Users;
	}

	@Override
	public Users findUsersById(int usersId) {
		return userRepo.findById(usersId).get();
	}

	@Override
	public Users findByLogin(String login) {
		return userRepo.findByLogin(login);
	}

	@Override
	public Users findByMatricule(String matricule) {
		return userRepo.findByMatricule(matricule);
	}

	@Override
	public Users findByNumeroCni(String numeroCni) {
		return userRepo.findByNumeroCni(numeroCni);
	}

	@Override
	public Users findByTel(String tel) {
		return userRepo.findByTel(tel);
	}

	@Override
	public Users deleteUsers(int usersId) {
		return null;
	}
/*
	@Override
	public Users connectUsersWithLoginAndPass(Users user) {
		Users userToReturn= (Users) userRepo.connectUsersWithLoginAndPass(user.getLogin(), user.getMotPass(), Constants.STATE_ACTIVATED);
		return userToReturn;
	}
*/

	@Override
	public Users connectUser(Users user) {
		Users userToReturn= (Users) userRepo.connectUser(user.getLogin(), user.getMotPass(), Constants.STATE_ACTIVATED);
		return userToReturn;
	}
}
