package indent.gts.indentifcation.servicesImpl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import indent.gts.indentifcation.beans.Moto;
import indent.gts.indentifcation.repository.MotoRepository;
import indent.gts.indentifcation.services.MotoService;
@Service
@Transactional
public class MotoServiceImpl implements MotoService{
	
	@Autowired
	MotoRepository motoRepo;

	@Override
	public Moto createMoto(Moto moto) {
		moto.setCreateAt(new Date());
		return motoRepo.save(moto);
	}

	@Override
	public Moto updateMoto(Moto moto) {
		return motoRepo.save(moto);
	}

	@Override
	public List<Moto> getAllMotos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Moto deleteMoto(int motoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Moto findMotoById(int motoId) {
		// TODO Auto-generated method stub
		return null;
	}

}
