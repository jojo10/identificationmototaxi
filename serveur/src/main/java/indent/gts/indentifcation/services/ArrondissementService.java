package indent.gts.indentifcation.services;

import java.util.List;

import indent.gts.indentifcation.beans.Arrondissement;
import indent.gts.indentifcation.beans.Users;

public interface ArrondissementService {

	public List<Arrondissement> getAllArrondissements ();
	//public List<Arrondissement> getAllArrondissementByDepartements (int departementId);
}
