package indent.gts.indentifcation.services;

import java.util.List;

import indent.gts.indentifcation.beans.Moto;
import indent.gts.indentifcation.beans.Users;

public interface MotoService {

	public Moto createMoto(Moto moto);
	public Moto updateMoto(Moto moto);
	public List<Moto> getAllMotos ();
	public Moto deleteMoto(int motoId);
	public Moto findMotoById(int motoId);

}
