package indent.gts.indentifcation.services;

import java.util.List;

import indent.gts.indentifcation.beans.UsersArrondissement;

public interface UsersArrondissementService {

	public List<UsersArrondissement> getAllUsersArrondissements ();
	public UsersArrondissement createUsersArrondissement(UsersArrondissement UsersArrondissement);
	public UsersArrondissement updateUsersArrondissement(UsersArrondissement UsersArrondissement);
}
