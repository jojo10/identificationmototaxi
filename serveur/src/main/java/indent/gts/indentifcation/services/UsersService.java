package indent.gts.indentifcation.services;

import java.util.List;
import indent.gts.indentifcation.beans.Users;

public interface UsersService {
	
	public Users createUsers(Users user);
	public Users updateUsers(Users user);
	public List<Users> getAllUsers ();
	public Users deleteUsers(int usersId);
	public Users findUsersById(int usersId);
	public Users findByLogin(String login);
	public Users findByMatricule(String matricule);
	public Users findByNumeroCni (String numeroCni);
	public Users findByTel(String tel);
	public Users connectUser(Users user);

}
