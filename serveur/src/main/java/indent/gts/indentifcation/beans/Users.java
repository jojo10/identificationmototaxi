/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "users")
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findByUsersId", query = "SELECT u FROM Users u WHERE u.usersId = :usersId"),
    @NamedQuery(name = "Users.findByUsername", query = "SELECT u FROM Users u WHERE u.username = :username"),
    @NamedQuery(name = "Users.findByPrenom", query = "SELECT u FROM Users u WHERE u.prenom = :prenom"),
    @NamedQuery(name = "Users.findByLieuNaissance", query = "SELECT u FROM Users u WHERE u.lieuNaissance = :lieuNaissance"),
    @NamedQuery(name = "Users.findByDateNaissance", query = "SELECT u FROM Users u WHERE u.dateNaissance = :dateNaissance"),
    @NamedQuery(name = "Users.findByNumeroCni", query = "SELECT u FROM Users u WHERE u.numeroCni = :numeroCni"),
    @NamedQuery(name = "Users.findByMatricule", query = "SELECT u FROM Users u WHERE u.matricule = :matricule"),
    @NamedQuery(name = "Users.findByLogin", query = "SELECT u FROM Users u WHERE u.login = :login"),
    @NamedQuery(name = "Users.findByMotPass", query = "SELECT u FROM Users u WHERE u.motPass = :motPass"),
    @NamedQuery(name = "Users.findBySituationMatrimoniale", query = "SELECT u FROM Users u WHERE u.situationMatrimoniale = :situationMatrimoniale"),
    @NamedQuery(name = "Users.findBySexe", query = "SELECT u FROM Users u WHERE u.sexe = :sexe"),
    @NamedQuery(name = "Users.findByTel", query = "SELECT u FROM Users u WHERE u.tel = :tel"),
    @NamedQuery(name = "Users.findByTelUrgence", query = "SELECT u FROM Users u WHERE u.telUrgence = :telUrgence"),
    @NamedQuery(name = "Users.findByNumeroPermis", query = "SELECT u FROM Users u WHERE u.numeroPermis = :numeroPermis"),
    @NamedQuery(name = "Users.findByCreateAt", query = "SELECT u FROM Users u WHERE u.createAt = :createAt"),
    @NamedQuery(name = "Users.findByUsermail", query = "SELECT u FROM Users u WHERE u.usermail = :usermail"),
    @NamedQuery(name = "Users.findByStatus", query = "SELECT u FROM Users u WHERE u.status = :status")})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "users_id")
    private Integer usersId;
    @Column(name = "username")
    private String username;
    @Column(name = "prenom")
    private String prenom;
    @Basic(optional = false)
    @Column(name = "lieu_naissance")
    private String lieuNaissance;
    @Basic(optional = false)
    @Column(name = "date_naissance")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateNaissance;
    @Column(name = "numero_cni")
    private String numeroCni;
    @Column(name = "matricule")
    private String matricule;
    @Basic(optional = false)
    @Column(name = "login")
    private String login;
    @Column(name = "mot_pass")
    private String motPass;
    @Column(name = "situation_matrimoniale")
    private String situationMatrimoniale;
    @Column(name = "sexe")
    private String sexe;
    @Basic(optional = false)
    @Column(name = "tel")
    private String tel;
    @Column(name = "tel_urgence")
    private String telUrgence;
    @Column(name = "numero_permis")
    private String numeroPermis;
    @Basic(optional = false)
    @Column(name = "create_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name = "usermail")
    private String usermail;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usersId", fetch = FetchType.LAZY)
    private List<Moto> motoList;
    @JoinColumn(name = "arrodisement_id", referencedColumnName = "arrodisement_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Arrondissement arrodisementId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usersId", fetch = FetchType.LAZY)
    private List<Alerts> alertsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users", fetch = FetchType.LAZY)
    private List<UsersArrondissement> usersArrondissementList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usersId", fetch = FetchType.LAZY)
    private List<Cotisation> cotisationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usersId", fetch = FetchType.LAZY)
    private List<Doc> docList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "users", fetch = FetchType.LAZY)
    private List<UsersGroup> usersGroupList;

    public Users() {
    }

    public Users(Integer usersId) {
        this.usersId = usersId;
    }

    public Users(Integer usersId, String lieuNaissance, Date dateNaissance, String login, String tel, Date createAt, short status) {
        this.usersId = usersId;
        this.lieuNaissance = lieuNaissance;
        this.dateNaissance = dateNaissance;
        this.login = login;
        this.tel = tel;
        this.createAt = createAt;
        this.status = status;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getNumeroCni() {
        return numeroCni;
    }

    public void setNumeroCni(String numeroCni) {
        this.numeroCni = numeroCni;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotPass() {
        return motPass;
    }

    public void setMotPass(String motPass) {
        this.motPass = motPass;
    }

    public String getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(String situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTelUrgence() {
        return telUrgence;
    }

    public void setTelUrgence(String telUrgence) {
        this.telUrgence = telUrgence;
    }

    public String getNumeroPermis() {
        return numeroPermis;
    }

    public void setNumeroPermis(String numeroPermis) {
        this.numeroPermis = numeroPermis;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getUsermail() {
        return usermail;
    }

    public void setUsermail(String usermail) {
        this.usermail = usermail;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public List<Moto> getMotoList() {
        return motoList;
    }

    public void setMotoList(List<Moto> motoList) {
        this.motoList = motoList;
    }

    public Arrondissement getArrodisementId() {
        return arrodisementId;
    }

    public void setArrodisementId(Arrondissement arrodisementId) {
        this.arrodisementId = arrodisementId;
    }

    public List<Alerts> getAlertsList() {
        return alertsList;
    }

    public void setAlertsList(List<Alerts> alertsList) {
        this.alertsList = alertsList;
    }

    public List<UsersArrondissement> getUsersArrondissementList() {
        return usersArrondissementList;
    }

    public void setUsersArrondissementList(List<UsersArrondissement> usersArrondissementList) {
        this.usersArrondissementList = usersArrondissementList;
    }

    public List<Cotisation> getCotisationList() {
        return cotisationList;
    }

    public void setCotisationList(List<Cotisation> cotisationList) {
        this.cotisationList = cotisationList;
    }

    public List<Doc> getDocList() {
        return docList;
    }

    public void setDocList(List<Doc> docList) {
        this.docList = docList;
    }

    public List<UsersGroup> getUsersGroupList() {
        return usersGroupList;
    }

    public void setUsersGroupList(List<UsersGroup> usersGroupList) {
        this.usersGroupList = usersGroupList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usersId != null ? usersId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.usersId == null && other.usersId != null) || (this.usersId != null && !this.usersId.equals(other.usersId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Users[ usersId=" + usersId + " ]";
    }
    
}
