/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "users_group")
@NamedQueries({
    @NamedQuery(name = "UsersGroup.findAll", query = "SELECT u FROM UsersGroup u"),
    @NamedQuery(name = "UsersGroup.findByUsersId", query = "SELECT u FROM UsersGroup u WHERE u.usersGroupPK.usersId = :usersId"),
    @NamedQuery(name = "UsersGroup.findByGroupId", query = "SELECT u FROM UsersGroup u WHERE u.usersGroupPK.groupId = :groupId"),
    @NamedQuery(name = "UsersGroup.findByFormDate", query = "SELECT u FROM UsersGroup u WHERE u.usersGroupPK.formDate = :formDate"),
    @NamedQuery(name = "UsersGroup.findByEndDate", query = "SELECT u FROM UsersGroup u WHERE u.endDate = :endDate")})
public class UsersGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsersGroupPK usersGroupPK;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @JoinColumn(name = "group_id", referencedColumnName = "group_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Groupes groupes;
    @JoinColumn(name = "users_id", referencedColumnName = "users_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users users;

    public UsersGroup() {
    }

    public UsersGroup(UsersGroupPK usersGroupPK) {
        this.usersGroupPK = usersGroupPK;
    }

    public UsersGroup(long usersId, long groupId, Date formDate) {
        this.usersGroupPK = new UsersGroupPK(usersId, groupId, formDate);
    }

    public UsersGroupPK getUsersGroupPK() {
        return usersGroupPK;
    }

    public void setUsersGroupPK(UsersGroupPK usersGroupPK) {
        this.usersGroupPK = usersGroupPK;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Groupes getGroupes() {
        return groupes;
    }

    public void setGroupes(Groupes groupes) {
        this.groupes = groupes;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usersGroupPK != null ? usersGroupPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersGroup)) {
            return false;
        }
        UsersGroup other = (UsersGroup) object;
        if ((this.usersGroupPK == null && other.usersGroupPK != null) || (this.usersGroupPK != null && !this.usersGroupPK.equals(other.usersGroupPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.UsersGroup[ usersGroupPK=" + usersGroupPK + " ]";
    }
    
}
