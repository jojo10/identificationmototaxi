/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "doc")
@NamedQueries({
    @NamedQuery(name = "Doc.findAll", query = "SELECT d FROM Doc d"),
    @NamedQuery(name = "Doc.findByDocId", query = "SELECT d FROM Doc d WHERE d.docId = :docId"),
    @NamedQuery(name = "Doc.findByDocName", query = "SELECT d FROM Doc d WHERE d.docName = :docName"),
    @NamedQuery(name = "Doc.findByDocPath", query = "SELECT d FROM Doc d WHERE d.docPath = :docPath"),
    @NamedQuery(name = "Doc.findByExtension", query = "SELECT d FROM Doc d WHERE d.extension = :extension"),
    @NamedQuery(name = "Doc.findByDocType", query = "SELECT d FROM Doc d WHERE d.docType = :docType"),
    @NamedQuery(name = "Doc.findByCreatedAt", query = "SELECT d FROM Doc d WHERE d.createdAt = :createdAt"),
    @NamedQuery(name = "Doc.findByStatus", query = "SELECT d FROM Doc d WHERE d.status = :status")})
public class Doc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "doc_id")
    private Integer docId;
    @Basic(optional = false)
    @Column(name = "doc_name")
    private String docName;
    @Column(name = "doc_path")
    private String docPath;
    @Column(name = "extension")
    private String extension;
    @Column(name = "doc_type")
    private String docType;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "users_id", referencedColumnName = "users_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users usersId;

    public Doc() {
    }

    public Doc(Integer docId) {
        this.docId = docId;
    }

    public Doc(Integer docId, String docName, short status) {
        this.docId = docId;
        this.docName = docName;
        this.status = status;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (docId != null ? docId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Doc)) {
            return false;
        }
        Doc other = (Doc) object;
        if ((this.docId == null && other.docId != null) || (this.docId != null && !this.docId.equals(other.docId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Doc[ docId=" + docId + " ]";
    }
    
}
