/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Embeddable
public class UsersArrondissementPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "arrodisement_id")
    private long arrodisementId;
    @Basic(optional = false)
    @Column(name = "users_id")
    private long usersId;
    @Basic(optional = false)
    @Column(name = "from_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;

    public UsersArrondissementPK() {
    }

    public UsersArrondissementPK(long arrodisementId, long usersId, Date fromDate) {
        this.arrodisementId = arrodisementId;
        this.usersId = usersId;
        this.fromDate = fromDate;
    }

    public long getArrodisementId() {
        return arrodisementId;
    }

    public void setArrodisementId(long arrodisementId) {
        this.arrodisementId = arrodisementId;
    }

    public long getUsersId() {
        return usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) arrodisementId;
        hash += (int) usersId;
        hash += (fromDate != null ? fromDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersArrondissementPK)) {
            return false;
        }
        UsersArrondissementPK other = (UsersArrondissementPK) object;
        if (this.arrodisementId != other.arrodisementId) {
            return false;
        }
        if (this.usersId != other.usersId) {
            return false;
        }
        if ((this.fromDate == null && other.fromDate != null) || (this.fromDate != null && !this.fromDate.equals(other.fromDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.UsersArrondissementPK[ arrodisementId=" + arrodisementId + ", usersId=" + usersId + ", fromDate=" + fromDate + " ]";
    }
    
}
