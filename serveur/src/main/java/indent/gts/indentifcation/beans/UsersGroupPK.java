/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Embeddable
public class UsersGroupPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "users_id")
    private long usersId;
    @Basic(optional = false)
    @Column(name = "group_id")
    private long groupId;
    @Basic(optional = false)
    @Column(name = "form_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date formDate;

    public UsersGroupPK() {
    }

    public UsersGroupPK(long usersId, long groupId, Date formDate) {
        this.usersId = usersId;
        this.groupId = groupId;
        this.formDate = formDate;
    }

    public long getUsersId() {
        return usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public Date getFormDate() {
        return formDate;
    }

    public void setFormDate(Date formDate) {
        this.formDate = formDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) usersId;
        hash += (int) groupId;
        hash += (formDate != null ? formDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersGroupPK)) {
            return false;
        }
        UsersGroupPK other = (UsersGroupPK) object;
        if (this.usersId != other.usersId) {
            return false;
        }
        if (this.groupId != other.groupId) {
            return false;
        }
        if ((this.formDate == null && other.formDate != null) || (this.formDate != null && !this.formDate.equals(other.formDate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.UsersGroupPK[ usersId=" + usersId + ", groupId=" + groupId + ", formDate=" + formDate + " ]";
    }
    
}
