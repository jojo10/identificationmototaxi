/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "moto")
@NamedQueries({
    @NamedQuery(name = "Moto.findAll", query = "SELECT m FROM Moto m"),
    @NamedQuery(name = "Moto.findByMotoId", query = "SELECT m FROM Moto m WHERE m.motoId = :motoId"),
    @NamedQuery(name = "Moto.findByMarqueMoto", query = "SELECT m FROM Moto m WHERE m.marqueMoto = :marqueMoto"),
    @NamedQuery(name = "Moto.findByImmatriculation", query = "SELECT m FROM Moto m WHERE m.immatriculation = :immatriculation"),
    @NamedQuery(name = "Moto.findByMotoName", query = "SELECT m FROM Moto m WHERE m.motoName = :motoName"),
    @NamedQuery(name = "Moto.findByCreateAt", query = "SELECT m FROM Moto m WHERE m.createAt = :createAt"),
    @NamedQuery(name = "Moto.findByStatus", query = "SELECT m FROM Moto m WHERE m.status = :status")})
public class Moto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "moto_id")
    private Integer motoId;
    @Column(name = "marque_moto")
    private String marqueMoto;
    @Column(name = "immatriculation")
    private String immatriculation;
    @Column(name = "moto_name")
    private String motoName;
    @Basic(optional = false)
    @Column(name = "create_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "users_id", referencedColumnName = "users_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users usersId;

    public Moto() {
    }

    public Moto(Integer motoId) {
        this.motoId = motoId;
    }

    public Moto(Integer motoId, Date createAt, short status) {
        this.motoId = motoId;
        this.createAt = createAt;
        this.status = status;
    }

    public Integer getMotoId() {
        return motoId;
    }

    public void setMotoId(Integer motoId) {
        this.motoId = motoId;
    }

    public String getMarqueMoto() {
        return marqueMoto;
    }

    public void setMarqueMoto(String marqueMoto) {
        this.marqueMoto = marqueMoto;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public String getMotoName() {
        return motoName;
    }

    public void setMotoName(String motoName) {
        this.motoName = motoName;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (motoId != null ? motoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Moto)) {
            return false;
        }
        Moto other = (Moto) object;
        if ((this.motoId == null && other.motoId != null) || (this.motoId != null && !this.motoId.equals(other.motoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Moto[ motoId=" + motoId + " ]";
    }
    
}
