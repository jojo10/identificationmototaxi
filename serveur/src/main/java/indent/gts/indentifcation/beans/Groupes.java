/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "groupes")
@NamedQueries({
    @NamedQuery(name = "Groupes.findAll", query = "SELECT g FROM Groupes g"),
    @NamedQuery(name = "Groupes.findByGroupId", query = "SELECT g FROM Groupes g WHERE g.groupId = :groupId"),
    @NamedQuery(name = "Groupes.findByGroupname", query = "SELECT g FROM Groupes g WHERE g.groupname = :groupname"),
    @NamedQuery(name = "Groupes.findByDescription", query = "SELECT g FROM Groupes g WHERE g.description = :description"),
    @NamedQuery(name = "Groupes.findByCreatedAt", query = "SELECT g FROM Groupes g WHERE g.createdAt = :createdAt"),
    @NamedQuery(name = "Groupes.findByStatus", query = "SELECT g FROM Groupes g WHERE g.status = :status")})
public class Groupes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "group_id")
    private Integer groupId;
    @Basic(optional = false)
    @Column(name = "groupname")
    private String groupname;
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupes", fetch = FetchType.LAZY)
    private List<RolesGroup> rolesGroupList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupes", fetch = FetchType.LAZY)
    private List<UsersGroup> usersGroupList;

    public Groupes() {
    }

    public Groupes(Integer groupId) {
        this.groupId = groupId;
    }

    public Groupes(Integer groupId, String groupname, Date createdAt, short status) {
        this.groupId = groupId;
        this.groupname = groupname;
        this.createdAt = createdAt;
        this.status = status;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public List<RolesGroup> getRolesGroupList() {
        return rolesGroupList;
    }

    public void setRolesGroupList(List<RolesGroup> rolesGroupList) {
        this.rolesGroupList = rolesGroupList;
    }

    public List<UsersGroup> getUsersGroupList() {
        return usersGroupList;
    }

    public void setUsersGroupList(List<UsersGroup> usersGroupList) {
        this.usersGroupList = usersGroupList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Groupes)) {
            return false;
        }
        Groupes other = (Groupes) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Groupes[ groupId=" + groupId + " ]";
    }
    
}
