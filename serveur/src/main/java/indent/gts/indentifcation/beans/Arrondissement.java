/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "arrondissement")
@NamedQueries({
    @NamedQuery(name = "Arrondissement.findAll", query = "SELECT a FROM Arrondissement a"),
    @NamedQuery(name = "Arrondissement.findByArrodisementId", query = "SELECT a FROM Arrondissement a WHERE a.arrodisementId = :arrodisementId"),
    @NamedQuery(name = "Arrondissement.findByArrondissementName", query = "SELECT a FROM Arrondissement a WHERE a.arrondissementName = :arrondissementName"),
    @NamedQuery(name = "Arrondissement.findByDescription", query = "SELECT a FROM Arrondissement a WHERE a.description = :description"),
    @NamedQuery(name = "Arrondissement.findByCreatedAt", query = "SELECT a FROM Arrondissement a WHERE a.createdAt = :createdAt"),
    @NamedQuery(name = "Arrondissement.findByStatus", query = "SELECT a FROM Arrondissement a WHERE a.status = :status")})
public class Arrondissement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "arrodisement_id")
    private Integer arrodisementId;
    @Basic(optional = false)
    @Column(name = "arrondissement_name")
    private String arrondissementName;
    @Column(name = "description")
    private String description;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "departement_id", referencedColumnName = "departement_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Departement departementId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "arrodisementId", fetch = FetchType.LAZY)
    private List<Users> usersList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "arrondissement", fetch = FetchType.LAZY)
    private List<UsersArrondissement> usersArrondissementList;

    public Arrondissement() {
    }

    public Arrondissement(Integer arrodisementId) {
        this.arrodisementId = arrodisementId;
    }

    public Arrondissement(Integer arrodisementId, String arrondissementName, short status) {
        this.arrodisementId = arrodisementId;
        this.arrondissementName = arrondissementName;
        this.status = status;
    }

    public Integer getArrodisementId() {
        return arrodisementId;
    }

    public void setArrodisementId(Integer arrodisementId) {
        this.arrodisementId = arrodisementId;
    }

    public String getArrondissementName() {
        return arrondissementName;
    }

    public void setArrondissementName(String arrondissementName) {
        this.arrondissementName = arrondissementName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Departement getDepartementId() {
        return departementId;
    }

    public void setDepartementId(Departement departementId) {
        this.departementId = departementId;
    }

    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    public List<UsersArrondissement> getUsersArrondissementList() {
        return usersArrondissementList;
    }

    public void setUsersArrondissementList(List<UsersArrondissement> usersArrondissementList) {
        this.usersArrondissementList = usersArrondissementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arrodisementId != null ? arrodisementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Arrondissement)) {
            return false;
        }
        Arrondissement other = (Arrondissement) object;
        if ((this.arrodisementId == null && other.arrodisementId != null) || (this.arrodisementId != null && !this.arrodisementId.equals(other.arrodisementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Arrondissement[ arrodisementId=" + arrodisementId + " ]";
    }
    
}
