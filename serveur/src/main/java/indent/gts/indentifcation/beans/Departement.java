/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "departement")
@NamedQueries({
    @NamedQuery(name = "Departement.findAll", query = "SELECT d FROM Departement d"),
    @NamedQuery(name = "Departement.findByDepartementId", query = "SELECT d FROM Departement d WHERE d.departementId = :departementId"),
    @NamedQuery(name = "Departement.findByDepartementName", query = "SELECT d FROM Departement d WHERE d.departementName = :departementName"),
    @NamedQuery(name = "Departement.findByDescription", query = "SELECT d FROM Departement d WHERE d.description = :description"),
    @NamedQuery(name = "Departement.findByCreateAt", query = "SELECT d FROM Departement d WHERE d.createAt = :createAt"),
    @NamedQuery(name = "Departement.findByStatus", query = "SELECT d FROM Departement d WHERE d.status = :status")})
public class Departement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "departement_id")
    private Integer departementId;
    @Basic(optional = false)
    @Column(name = "departement_name")
    private String departementName;
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "create_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "region_id", referencedColumnName = "region_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Region regionId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departementId", fetch = FetchType.LAZY)
    private List<Arrondissement> arrondissementList;

    public Departement() {
    }

    public Departement(Integer departementId) {
        this.departementId = departementId;
    }

    public Departement(Integer departementId, String departementName, Date createAt, short status) {
        this.departementId = departementId;
        this.departementName = departementName;
        this.createAt = createAt;
        this.status = status;
    }

    public Integer getDepartementId() {
        return departementId;
    }

    public void setDepartementId(Integer departementId) {
        this.departementId = departementId;
    }

    public String getDepartementName() {
        return departementName;
    }

    public void setDepartementName(String departementName) {
        this.departementName = departementName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Region getRegionId() {
        return regionId;
    }

    public void setRegionId(Region regionId) {
        this.regionId = regionId;
    }

    public List<Arrondissement> getArrondissementList() {
        return arrondissementList;
    }

    public void setArrondissementList(List<Arrondissement> arrondissementList) {
        this.arrondissementList = arrondissementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (departementId != null ? departementId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Departement)) {
            return false;
        }
        Departement other = (Departement) object;
        if ((this.departementId == null && other.departementId != null) || (this.departementId != null && !this.departementId.equals(other.departementId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Departement[ departementId=" + departementId + " ]";
    }
    
}
