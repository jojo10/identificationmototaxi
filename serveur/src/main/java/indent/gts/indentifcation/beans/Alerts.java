/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "alerts")
@NamedQueries({
    @NamedQuery(name = "Alerts.findAll", query = "SELECT a FROM Alerts a"),
    @NamedQuery(name = "Alerts.findByAlertId", query = "SELECT a FROM Alerts a WHERE a.alertId = :alertId"),
    @NamedQuery(name = "Alerts.findByAlertsMessage", query = "SELECT a FROM Alerts a WHERE a.alertsMessage = :alertsMessage"),
    @NamedQuery(name = "Alerts.findByAlertsDescription", query = "SELECT a FROM Alerts a WHERE a.alertsDescription = :alertsDescription"),
    @NamedQuery(name = "Alerts.findByAlertsDistrict", query = "SELECT a FROM Alerts a WHERE a.alertsDistrict = :alertsDistrict"),
    @NamedQuery(name = "Alerts.findByAlertsDate", query = "SELECT a FROM Alerts a WHERE a.alertsDate = :alertsDate"),
    @NamedQuery(name = "Alerts.findByStatus", query = "SELECT a FROM Alerts a WHERE a.status = :status")})
public class Alerts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "alert_id")
    private Integer alertId;
    @Basic(optional = false)
    @Column(name = "alerts_message")
    private String alertsMessage;
    @Column(name = "alerts_description")
    private String alertsDescription;
    @Basic(optional = false)
    @Column(name = "alerts_district")
    private String alertsDistrict;
    @Basic(optional = false)
    @Column(name = "alerts_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date alertsDate;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "users_id", referencedColumnName = "users_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users usersId;

    public Alerts() {
    }

    public Alerts(Integer alertId) {
        this.alertId = alertId;
    }

    public Alerts(Integer alertId, String alertsMessage, String alertsDistrict, Date alertsDate, short status) {
        this.alertId = alertId;
        this.alertsMessage = alertsMessage;
        this.alertsDistrict = alertsDistrict;
        this.alertsDate = alertsDate;
        this.status = status;
    }

    public Integer getAlertId() {
        return alertId;
    }

    public void setAlertId(Integer alertId) {
        this.alertId = alertId;
    }

    public String getAlertsMessage() {
        return alertsMessage;
    }

    public void setAlertsMessage(String alertsMessage) {
        this.alertsMessage = alertsMessage;
    }

    public String getAlertsDescription() {
        return alertsDescription;
    }

    public void setAlertsDescription(String alertsDescription) {
        this.alertsDescription = alertsDescription;
    }

    public String getAlertsDistrict() {
        return alertsDistrict;
    }

    public void setAlertsDistrict(String alertsDistrict) {
        this.alertsDistrict = alertsDistrict;
    }

    public Date getAlertsDate() {
        return alertsDate;
    }

    public void setAlertsDate(Date alertsDate) {
        this.alertsDate = alertsDate;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alertId != null ? alertId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alerts)) {
            return false;
        }
        Alerts other = (Alerts) object;
        if ((this.alertId == null && other.alertId != null) || (this.alertId != null && !this.alertId.equals(other.alertId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Alerts[ alertId=" + alertId + " ]";
    }
    
}
