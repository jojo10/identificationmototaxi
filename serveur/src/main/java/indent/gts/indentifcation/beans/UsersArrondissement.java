/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "users_arrondissement")
@NamedQueries({
    @NamedQuery(name = "UsersArrondissement.findAll", query = "SELECT u FROM UsersArrondissement u"),
    @NamedQuery(name = "UsersArrondissement.findByArrodisementId", query = "SELECT u FROM UsersArrondissement u WHERE u.usersArrondissementPK.arrodisementId = :arrodisementId"),
    @NamedQuery(name = "UsersArrondissement.findByUsersId", query = "SELECT u FROM UsersArrondissement u WHERE u.usersArrondissementPK.usersId = :usersId"),
    @NamedQuery(name = "UsersArrondissement.findByFromDate", query = "SELECT u FROM UsersArrondissement u WHERE u.usersArrondissementPK.fromDate = :fromDate"),
    @NamedQuery(name = "UsersArrondissement.findByEndDate", query = "SELECT u FROM UsersArrondissement u WHERE u.endDate = :endDate")})
public class UsersArrondissement implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsersArrondissementPK usersArrondissementPK;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @JoinColumn(name = "arrodisement_id", referencedColumnName = "arrodisement_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Arrondissement arrondissement;
    @JoinColumn(name = "users_id", referencedColumnName = "users_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users users;

    public UsersArrondissement() {
    }

    public UsersArrondissement(UsersArrondissementPK usersArrondissementPK) {
        this.usersArrondissementPK = usersArrondissementPK;
    }

    public UsersArrondissement(long arrodisementId, long usersId, Date fromDate) {
        this.usersArrondissementPK = new UsersArrondissementPK(arrodisementId, usersId, fromDate);
    }

    public UsersArrondissementPK getUsersArrondissementPK() {
        return usersArrondissementPK;
    }

    public void setUsersArrondissementPK(UsersArrondissementPK usersArrondissementPK) {
        this.usersArrondissementPK = usersArrondissementPK;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Arrondissement getArrondissement() {
        return arrondissement;
    }

    public void setArrondissement(Arrondissement arrondissement) {
        this.arrondissement = arrondissement;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usersArrondissementPK != null ? usersArrondissementPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersArrondissement)) {
            return false;
        }
        UsersArrondissement other = (UsersArrondissement) object;
        if ((this.usersArrondissementPK == null && other.usersArrondissementPK != null) || (this.usersArrondissementPK != null && !this.usersArrondissementPK.equals(other.usersArrondissementPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.UsersArrondissement[ usersArrondissementPK=" + usersArrondissementPK + " ]";
    }
    
}
