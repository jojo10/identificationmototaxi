/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "roles_group")
@NamedQueries({
    @NamedQuery(name = "RolesGroup.findAll", query = "SELECT r FROM RolesGroup r"),
    @NamedQuery(name = "RolesGroup.findByGroupId", query = "SELECT r FROM RolesGroup r WHERE r.rolesGroupPK.groupId = :groupId"),
    @NamedQuery(name = "RolesGroup.findByRolesId", query = "SELECT r FROM RolesGroup r WHERE r.rolesGroupPK.rolesId = :rolesId"),
    @NamedQuery(name = "RolesGroup.findByCreatedAt", query = "SELECT r FROM RolesGroup r WHERE r.rolesGroupPK.createdAt = :createdAt"),
    @NamedQuery(name = "RolesGroup.findByEndDate", query = "SELECT r FROM RolesGroup r WHERE r.endDate = :endDate")})
public class RolesGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RolesGroupPK rolesGroupPK;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @JoinColumn(name = "group_id", referencedColumnName = "group_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Groupes groupes;
    @JoinColumn(name = "roles_id", referencedColumnName = "roles_id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Roles roles;

    public RolesGroup() {
    }

    public RolesGroup(RolesGroupPK rolesGroupPK) {
        this.rolesGroupPK = rolesGroupPK;
    }

    public RolesGroup(long groupId, long rolesId, Date createdAt) {
        this.rolesGroupPK = new RolesGroupPK(groupId, rolesId, createdAt);
    }

    public RolesGroupPK getRolesGroupPK() {
        return rolesGroupPK;
    }

    public void setRolesGroupPK(RolesGroupPK rolesGroupPK) {
        this.rolesGroupPK = rolesGroupPK;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Groupes getGroupes() {
        return groupes;
    }

    public void setGroupes(Groupes groupes) {
        this.groupes = groupes;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rolesGroupPK != null ? rolesGroupPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolesGroup)) {
            return false;
        }
        RolesGroup other = (RolesGroup) object;
        if ((this.rolesGroupPK == null && other.rolesGroupPK != null) || (this.rolesGroupPK != null && !this.rolesGroupPK.equals(other.rolesGroupPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.RolesGroup[ rolesGroupPK=" + rolesGroupPK + " ]";
    }
    
}
