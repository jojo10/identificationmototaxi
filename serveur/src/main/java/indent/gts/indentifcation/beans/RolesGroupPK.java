/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Embeddable
public class RolesGroupPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "group_id")
    private long groupId;
    @Basic(optional = false)
    @Column(name = "roles_id")
    private long rolesId;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public RolesGroupPK() {
    }

    public RolesGroupPK(long groupId, long rolesId, Date createdAt) {
        this.groupId = groupId;
        this.rolesId = rolesId;
        this.createdAt = createdAt;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getRolesId() {
        return rolesId;
    }

    public void setRolesId(long rolesId) {
        this.rolesId = rolesId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) groupId;
        hash += (int) rolesId;
        hash += (createdAt != null ? createdAt.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolesGroupPK)) {
            return false;
        }
        RolesGroupPK other = (RolesGroupPK) object;
        if (this.groupId != other.groupId) {
            return false;
        }
        if (this.rolesId != other.rolesId) {
            return false;
        }
        if ((this.createdAt == null && other.createdAt != null) || (this.createdAt != null && !this.createdAt.equals(other.createdAt))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.RolesGroupPK[ groupId=" + groupId + ", rolesId=" + rolesId + ", createdAt=" + createdAt + " ]";
    }
    
}
