/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indent.gts.indentifcation.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author SODJIO
 */
@Entity
@Table(name = "cotisation")
@NamedQueries({
    @NamedQuery(name = "Cotisation.findAll", query = "SELECT c FROM Cotisation c"),
    @NamedQuery(name = "Cotisation.findByCotisationId", query = "SELECT c FROM Cotisation c WHERE c.cotisationId = :cotisationId"),
    @NamedQuery(name = "Cotisation.findByMontant", query = "SELECT c FROM Cotisation c WHERE c.montant = :montant"),
    @NamedQuery(name = "Cotisation.findByLastupdate", query = "SELECT c FROM Cotisation c WHERE c.lastupdate = :lastupdate"),
    @NamedQuery(name = "Cotisation.findByCotisationDate", query = "SELECT c FROM Cotisation c WHERE c.cotisationDate = :cotisationDate"),
    @NamedQuery(name = "Cotisation.findByStatus", query = "SELECT c FROM Cotisation c WHERE c.status = :status")})
public class Cotisation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cotisation_id")
    private Integer cotisationId;
    @Basic(optional = false)
    @Column(name = "montant")
    private double montant;
    @Basic(optional = false)
    @Column(name = "lastupdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastupdate;
    @Basic(optional = false)
    @Column(name = "cotisation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotisationDate;
    @Basic(optional = false)
    @Column(name = "status")
    private short status;
    @JoinColumn(name = "users_id", referencedColumnName = "users_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users usersId;

    public Cotisation() {
    }

    public Cotisation(Integer cotisationId) {
        this.cotisationId = cotisationId;
    }

    public Cotisation(Integer cotisationId, double montant, Date lastupdate, Date cotisationDate, short status) {
        this.cotisationId = cotisationId;
        this.montant = montant;
        this.lastupdate = lastupdate;
        this.cotisationDate = cotisationDate;
        this.status = status;
    }

    public Integer getCotisationId() {
        return cotisationId;
    }

    public void setCotisationId(Integer cotisationId) {
        this.cotisationId = cotisationId;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public Date getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
    }

    public Date getCotisationDate() {
        return cotisationDate;
    }

    public void setCotisationDate(Date cotisationDate) {
        this.cotisationDate = cotisationDate;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cotisationId != null ? cotisationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cotisation)) {
            return false;
        }
        Cotisation other = (Cotisation) object;
        if ((this.cotisationId == null && other.cotisationId != null) || (this.cotisationId != null && !this.cotisationId.equals(other.cotisationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "idenMoto.Cotisation[ cotisationId=" + cotisationId + " ]";
    }
    
}
